1. Make sure you have installed MetaMask
2. Download MySQL and MySQL Workbench
3. Database Name: **bhsec_db**

3. `User` table

```
CREATE TABLE USERS (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT ,
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    public varchar(255) NOT NULL,
    private varchar(255) NOT NULL
);
```


`INSERT INTO USERS (username, password, public, private) VALUES ('bhsec', 'test123', '0x1C07DA50B6Cd3D7630442A79D7f48bF1BfdafF47', 'e4804d528b261aa206e0825391fefe09f2c67f5732395a122a0de48cd4c1960e');`

4. `USERS_DETAILS` table

```
CREATE TABLE USERS_DETAILS (
    fname varchar(255) NOT NULL,
    mname varchar(255),
    lname varchar(255),
    email varchar(255) NOT NULL,
    cid varchar(255) PRIMARY KEY NOT NULL,
    dob varchar(255) NOT NULL,
    school varchar(255) NOT NULL,
    stream varchar(255) NOT NULL,
    public varchar(255) NOT NULL,
    private varchar(255) NOT NULL
);
```


5. You can use the UI to enter student details
```
a. Student 1: 
Public: 0x40f5fade5a68e3086cb86390f167bdd643a87792
              Private: 36cd80ce5bb2ad8ba0af06c6eb0690970600303f1be5ee2cc6798afc15dab13c
b. Student 2: Public:
0x815f05d2263b5e09ed7b784c98e9cd0f85bc0d50
              Private: f508a7c69abf71ffcd72eb5b24f745bb5e61bb672972d535a3e78268fd3143b9
```


