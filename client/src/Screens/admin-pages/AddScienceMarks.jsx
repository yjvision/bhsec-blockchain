import React from "react";
import AdminHeader from "./AdminHeader";
import Web3 from "web3";
import Axios from "axios";
import Cookies from "universal-cookie";
import {
  SCIENCE_CONTRACT_ADDRESS,
  SCIENCE_ABI,
} from "../../abi/abi_studentScienceMarks";

const CryptoJS = require("crypto-js");
const secretPass = "XkhZG4fW2t2W";

class AddScienceMarks extends React.Component {
  // componentDidMount() method is called after the component is rendered
  componentDidMount() {
    this.loadBlockchainData();
  }

  // constructor
  // account: signed in address
  // user: student details
  constructor(props) {
    super(props);
    this.state = {
      account: "",
      status: "",
      user: [],
    };
    this.addMarks = this.addMarks.bind(this);
    this.populateDetails = this.populateDetails.bind(this);
  }

  // Function to populate details
  populateDetails(e) {
    Axios.get(`http://localhost:3001/get-one/${this.address.value}`).then(
      (response) => {
        if (response.data.error) {
          this.setState({ status: response.data.error });
        } else {
          this.setState({ ...this.state, user: response.data });
        }
      }
    );
  }

  // Create a web3 connection using a provider, MetaMask to connect
  // the ganache server and retrieve the first account
  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "http://localhost:7545");
    const accounts = await web3.eth.getAccounts();

    this.setState({ account: accounts[0] });
    // Load all the marks from the blockchain
    const studentMarks = new web3.eth.Contract(
      SCIENCE_ABI,
      SCIENCE_CONTRACT_ADDRESS
    );

    // Keep the marks in the current state
    this.setState({ studentMarks });
  }

  addMarks(
    address,
    english1,
    english2,
    dzongkha1,
    dzongkha2,
    chemistry,
    physics,
    biology,
    mathematics
  ) {
    // Check the address of admin
    const cookies = new Cookies();
    const ciphertext = cookies.get("cookies");

    // Decryption
    const bytes = CryptoJS.AES.decrypt(ciphertext, secretPass);
    const decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

    const loggedInAddress = decryptedData[0]["address"];
    const metamaskAddress = this.state.account.toLowerCase();

    if (loggedInAddress === metamaskAddress) {
      this.state.studentMarks.methods
        .registerStudentMarks(
          address,
          "Science",
          english1,
          english2,
          dzongkha1,
          dzongkha2,
          chemistry,
          physics,
          biology,
          mathematics
        )
        .send({ from: this.state.account })
        .once("receipt", (receipt) => {
          this.setState({ status: "Successfully added" });
        });
    } else {
      this.setState({ status: "You do not have right to add marks!" });
    }
  }

  render() {
    return (
      <React.Fragment>
        <AdminHeader></AdminHeader>
        <hr></hr>
        <h4 style={{ textAlign: "center" }}>Science Stream Marks Entry</h4>
        <hr></hr>
        {/* Add science marks */}

        <form
          onSubmit={(event) => {
            event.preventDefault();
            this.addMarks(
              this.address.value,
              this.english1.value,
              this.english2.value,
              this.dzongkha1.value,
              this.dzongkha2.value,
              this.chemistry.value,
              this.physics.value,
              this.biology.value,
              this.mathematics.value
            );
          }}
        >
          {/* <!-- 2 column grid layout with text inputs for the first and last names --> */}

          <div className="row mb-4">
            <div className="col">
              <div className="form-outline">
                <label className="form-label" for="form6Example1">
                  <b>You are entering marks for address</b>
                </label>
                <input
                  type="text"
                  required
                  ref={(input) => (this.address = input)}
                  onBlur={this.populateDetails.bind(this)}
                  placeholder="Public address"
                  className="form-control"
                />
              </div>
            </div>
          </div>

          <div className="row mb-4">
            {this.state.user.map((item) => (
              <div className="col">
                <div className="form-outline">
                  <label className="form-label" for="form6Example1">
                    <b>Student Name</b>
                  </label>
                  <input
                    type="text"
                    readOnly
                    value={item.fname + " " + " " + item.lname}
                    className="form-control"
                  />
                </div>
              </div>
            ))}

            {this.state.user.map((item) => (
              <div className="col">
                <div className="form-outline">
                  <label className="form-label" for="form6Example1">
                    <b>Index Number</b>
                  </label>
                  <input
                    type="text"
                    readOnly
                    value={item.indexN}
                    className="form-control"
                  />
                </div>
              </div>
            ))}

            {this.state.user.map((item) => (
              <div className="col">
                <div className="form-outline">
                  <label className="form-label" for="form6Example1">
                    <b>School</b>
                  </label>
                  <input
                    type="text"
                    readOnly
                    value={item.school}
                    className="form-control"
                  />
                </div>
              </div>
            ))}
          </div>
          <hr></hr>

          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form6Example1">
                  English-I
                </label>
                <input
                  type="number"
                  ref={(input) => (this.english1 = input)}
                  required
                  placeholder="English-I marks"
                  class="form-control"
                />
              </div>
            </div>

            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form6Example1">
                  English-II
                </label>
                <input
                  type="number"
                  ref={(input) => (this.english2 = input)}
                  placeholder="English-II marks"
                  class="form-control"
                />
              </div>
            </div>

            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form6Example2">
                  Dzongkha-I
                </label>
                <input
                  type="number"
                  ref={(input) => (this.dzongkha1 = input)}
                  placeholder="Dzongkha-I marks"
                  class="form-control"
                />
              </div>
            </div>

            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form6Example1">
                  Dzongkha-II
                </label>
                <input
                  type="number"
                  required
                  ref={(input) => (this.dzongkha2 = input)}
                  placeholder="Dzongkha-II marks"
                  class="form-control"
                />
              </div>
            </div>
          </div>

          <div class="row mb-4">
            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form6Example1">
                  Chemistry
                </label>
                <input
                  type="number"
                  required
                  ref={(input) => (this.chemistry = input)}
                  placeholder="Chemistry marks"
                  class="form-control"
                />
              </div>
            </div>

            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form6Example2">
                  Physics
                </label>
                <input
                  type="number"
                  required
                  ref={(input) => (this.physics = input)}
                  placeholder="Physics marks"
                  class="form-control"
                />
              </div>
            </div>

            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form6Example2">
                  Biology (Optional)
                </label>
                <input
                  type="text"
                  ref={(input) => (this.biology = input)}
                  placeholder="Biology marks"
                  class="form-control"
                />
              </div>
            </div>

            <div class="col">
              <div class="form-outline">
                <label class="form-label" for="form6Example2">
                  Mathematics (Optional)
                </label>
                <input
                  type="text"
                  ref={(input) => (this.mathematics = input)}
                  placeholder="Mathematics marks"
                  class="form-control"
                />
              </div>
            </div>
          </div>

          {/* <!-- Submit button --> */}
          <button type="submit" className="btn btn-primary">
            Add Marks
          </button>
        </form>
        <br></br>

        {/* /* Ternary Operator to show status */}
        {this.state.status ? (
          <div
            class="alert alert-success alert-dismissible fade show"
            role="alert"
          >
            {this.state.status}
            <button
              type="button"
              class="btn-close"
              data-bs-dismiss="alert"
              aria-label="Close"
            ></button>
          </div>
        ) : (
          <div>{this.state.status}</div>
        )}
      </React.Fragment>
    );
  }
}

export default AddScienceMarks;
