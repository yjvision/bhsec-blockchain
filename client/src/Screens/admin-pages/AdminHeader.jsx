import React from "react";
import "bootstrap/dist/js/bootstrap.min.js";
// import {useLocation} from 'react-router-dom';
import Cookies from "universal-cookie";

const CryptoJS = require("crypto-js");
const secretPass = "XkhZG4fW2t2W";

// class AdminHeader extends React.Component {
export default function AdminHeader() {
  const cookies = new Cookies();

  const ciphertext = cookies.get("cookies");

  // If the ciphertext is not defined, go to login page
  if (!ciphertext) {
    window.location.href = "/";
  }

  // Decryption
  var bytes = CryptoJS.AES.decrypt(ciphertext, secretPass);
  var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

  // assign the decrypted value
  var username = decryptedData[0]["username"];
  var address = decryptedData[0]["address"];

  const logout = () => {
    cookies.remove("cookies");
    window.location.href = "/";
  };

  return (
    <React.Fragment>
      <nav className="navbar navbar-expand-md bg-dark navbar-dark">
        <div className="container-fluid">
          <a className="navbar-brand" href="/admin">
            Admin Dashboard
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  School FP
                </a>
                <ul className="dropdown-menu">
                  <li>
                    <a className="dropdown-item" href="/add-school-focal">
                      Add
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="/view-focal-person">
                      View
                    </a>
                  </li>
                </ul>
              </li>

              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  School
                </a>
                <ul className="dropdown-menu">
                  <li>
                    <a className="dropdown-item" href="/add-school">
                      Add
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="/view-school">
                      View
                    </a>
                  </li>
                </ul>
              </li>

              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Marks Entry
                </a>
                <ul className="dropdown-menu">
                  <li>
                    <a className="dropdown-item" href="/add-science-marks">
                      Science
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="/add-commerce-marks">
                      Commerce
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="/add-arts-marks">
                      Arts
                    </a>
                  </li>
                </ul>
              </li>
            </ul>

            <button
              type="button"
              className="btn btn-danger btn-sm"
              onClick={logout}
            >
              Logout
            </button>
          </div>
        </div>
      </nav>
      <b>You are logged in as:</b> {username}
      <br></br>
      <b>Admin Public Address: </b> {address}
    </React.Fragment>
  );
}
