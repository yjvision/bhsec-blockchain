import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Axios from "axios";
import AdminHeader from "./AdminHeader";

export default function UpdateSchool() {
  const [school, setSchool] = useState("");
  const [dzongkhag, setDzongkhag] = useState("");
  const [status, setStatus] = useState("");

  const { sch } = useParams();

  useEffect(() => {
    Axios.get(`http://localhost:3001/getschool/${sch}`).then((response) => {
      setSchool(response.data[0].schoolName);
    });
  }, [sch]);

  // Update school
  const updateSchool = () => {
    Axios.put(`http://localhost:3001/putschool/${sch}`, {
      school: school,
      dzongkhag: dzongkhag,
    }).then((response) => {
      if (response.data.error) {
        setStatus(response.data.error);
      } else {
        setStatus(response.data.success);
        setTimeout(() => (window.location.href = "/view-school"), 500);
      }
    });
  };
  return (
    <React.Fragment>
      <AdminHeader></AdminHeader>
      <hr></hr>
      <h4 style={{ textAlign: "center" }}>Update School(s)</h4>
      <hr></hr>

      {/* Ternary Operator to show status */}
      {status ? (
        <div
          class="alert alert-success alert-dismissible fade show"
          role="alert"
        >
          {status}
          <button
            type="button"
            class="btn-close"
            data-bs-dismiss="alert"
            aria-label="Close"
          ></button>
        </div>
      ) : (
        <div>
          {status} <hr></hr>
        </div>
      )}

      <form>
        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example1">
                School Name
              </label>
              <input
                type="text"
                required
                value={school}
                placeholder="Enter School Name"
                onChange={(e) => setSchool(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Dzongkhag
              </label>
              <select
                required
                class="form-select"
                onChange={(e) => setDzongkhag(e.target.value)}
              >
                <option value="">Select Dzongkhag</option>
                <option value="Bumthang">Bumthang</option>
                <option value="Chukha">Chukha</option>
                <option value="Dagana">Dagana</option>
                <option value="Gasa">Gasa</option>
                <option value="Haa">Haa</option>
                <option value="Lhuntse">Lhuntse</option>
                <option value="Mongar">Mongar</option>
                <option value="Paro">Paro</option>
                <option value="Pemagatshel">Pemagatshel</option>
                <option value="Punakha">Punakha</option>
                <option value="Samdrup Jonkhar">Samdrup Jonkhar</option>
                <option value="Samtse">Samtse</option>
                <option value="Sarpang">Sarpang</option>
                <option value="Thimphu">Thimphu</option>
                <option value="Trashigang">Trashigang</option>
                <option value="Trashi Yangtse">Trashi Yangtse</option>
                <option value="Trongsa">Trongsa</option>
                <option value="Tsirang">Tsirang</option>
                <option value="Wangdue Phodrang<">Wangdue Phodrang</option>
                <option value="Zhemgang">Zhemgang</option>
              </select>
            </div>
          </div>
        </div>
        {/* <!-- Submit button --> */}
        <button
          onClick={updateSchool}
          type="button"
          className="btn btn-primary"
        >
          Update School
        </button>
      </form>
    </React.Fragment>
  );
}
