import React, { useState } from "react";
import AdminHeader from "./AdminHeader";
import Axios from "axios";
import { Link } from "react-router-dom";

export default function ViewSchool() {
  // state variables

  const [school, setSchool] = React.useState([]);
  const [status, setStatus] = useState("");

  // View School
  const viewDetails = (e) => {
    var dzongkhagName = e.target.value;
    Axios.get(`http://localhost:3001/get-school/${dzongkhagName}`).then(
      (response) => {
        if (response.data.error) {
          setStatus(response.data.error);
        } else {
          setSchool(response.data);
        }
      }
    );
  };

  return (
    <React.Fragment>
      <AdminHeader></AdminHeader>
      <hr></hr>
      <h4 style={{ textAlign: "center" }}>View School(s)</h4>
      <form>
        <div class="row mb-4">
          <div class="col">
            <div class="form-outline"></div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Select Dzongkhag
              </label>
              <select required class="form-select" onChange={viewDetails}>
                <option value="Bumthang">Bumthang</option>
                <option value="Chukha">Chukha</option>
                <option value="Dagana">Dagana</option>
                <option value="Gasa">Gasa</option>
                <option value="Haa">Haa</option>
                <option value="Lhuntse">Lhuntse</option>
                <option value="Mongar">Mongar</option>
                <option value="Paro">Paro</option>
                <option value="Pemagatshel">Pemagatshel</option>
                <option value="Punakha">Punakha</option>
                <option value="Samdrup Jonkhar">Samdrup Jonkhar</option>
                <option value="Samtse">Samtse</option>
                <option value="Sarpang">Sarpang</option>
                <option value="Thimphu">Thimphu</option>
                <option value="Trashigang">Trashigang</option>
                <option value="Trashi Yangtse">Trashi Yangtse</option>
                <option value="Trongsa">Trongsa</option>
                <option value="Tsirang">Tsirang</option>
                <option value="Wangdue Phodrang<">Wangdue Phodrang</option>
                <option value="Zhemgang">Zhemgang</option>
              </select>
            </div>
          </div>

          <div class="col">
            <div class="form-outline"></div>
          </div>
        </div>
      </form>

      <hr></hr>
      {/* Display the table data */}
      <div className="table-responsive">
        <table className="table table-hover table-striped" id="userTable">
          <thead className="table-light">
            <th>School</th>
            <th>Dzongkhag</th>
            <th>EDIT</th>
            <th>DELETE</th>
          </thead>

          <tbody>
            {school.map((school) => {
              return (
                <tr key={school.schoolName}>
                  <td>{school.schoolName}</td>
                  <td>{school.dzongkhag}</td>
                  <td>
                    <Link to={`/getschool/${school.schoolName}`}>
                      <button className="btn btn-primary btn-sm">Update</button>
                    </Link>
                  </td>
                  <td>
                    {/* <Link>
                      <button className="btn btn-danger btn-sm">Delete</button>
                    </Link> */}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </React.Fragment>
  );
}
