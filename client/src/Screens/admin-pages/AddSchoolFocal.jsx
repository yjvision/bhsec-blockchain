import React, { useEffect, useState } from "react";
import AdminHeader from "./AdminHeader";
import Axios from "axios";

function AddSchoolFocal() {
  // initial values
  const [empId, setEmpId] = useState("");
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [school, setSchool] = useState("");
  const [schools, setSchools] = useState([]);
  const [dzongkhag, setDzongkhag] = useState("");
  const [pri, setPrivate] = useState("");
  const [pub, setPublic] = useState("");
  const [status, setStatus] = useState("");

  useEffect(() => {
    Axios.get("http://localhost:3001/get-all-schools").then((response) => {
      setSchools(response.data);
    });
  }, []);

  // Add focal person function
  const addFocalPerson = (event) => {
    event.preventDefault(); // prevent page refresh
    // call server.js
    Axios.post("http://localhost:3001/add-focal", {
      empId: empId,
      name: name,
      password: password,
      email: email,
      school: school,
      dzongkhag: dzongkhag,
      pub: pub,
      pri: pri,
    }).then((response) => {
      if (response.data.error) {
        setStatus(response.data.error);
      } else {
        setStatus(response.data.success);
      }
    });
  };
  return (
    <React.Fragment>
      <AdminHeader></AdminHeader>
      <hr></hr>
      <h4 style={{ textAlign: "center" }}>Add School Focal Person</h4>
      <hr></hr>
      <form onSubmit={addFocalPerson}>
        {/* <!-- 2 column grid layout with text inputs for the first and last names --> */}
        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example1">
                Employee ID
              </label>
              <input
                type="text"
                required
                value={empId}
                placeholder="Enter Employee ID"
                onChange={(e) => setEmpId(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example1">
                Name
              </label>
              <input
                type="text"
                value={name}
                placeholder="Enter full name"
                onChange={(e) => setName(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Password
              </label>
              <input
                type="text"
                value={password}
                placeholder="Enter password"
                onChange={(e) => setPassword(e.target.value)}
                class="form-control"
              />
            </div>
          </div>
        </div>

        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example1">
                Email
              </label>
              <input
                type="email"
                required
                value={email}
                placeholder="Enter email"
                onChange={(e) => setEmail(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                School
              </label>
              <select
                required
                class="form-select"
                onChange={(e) => setSchool(e.target.value)}
              >
                <option value="">Select Dzongkhag</option>
                {schools.map((schools) => {
                  return (
                    <option value={schools.schoolName}>
                      {schools.schoolName}
                    </option>
                  );
                })}
              </select>
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Dzongkhag
              </label>
              <select
                required
                class="form-select"
                onChange={(e) => setDzongkhag(e.target.value)}
              >
                <option value="">Select Dzongkhag</option>
                <option value="Bumthang">Bumthang</option>
                <option value="Chukha">Chukha</option>
                <option value="Dagana">Dagana</option>
                <option value="Gasa">Gasa</option>
                <option value="Haa">Haa</option>
                <option value="Lhuntse">Lhuntse</option>
                <option value="Mongar">Mongar</option>
                <option value="Paro">Paro</option>
                <option value="Pemagatshel">Pemagatshel</option>
                <option value="Punakha">Punakha</option>
                <option value="Samdrup Jonkhar">Samdrup Jonkhar</option>
                <option value="Samtse">Samtse</option>
                <option value="Sarpang">Sarpang</option>
                <option value="Thimphu">Thimphu</option>
                <option value="Trashigang">Trashigang</option>
                <option value="Trashi Yangtse">Trashi Yangtse</option>
                <option value="Trongsa">Trongsa</option>
                <option value="Tsirang">Tsirang</option>
                <option value="Wangdue Phodrang<">Wangdue Phodrang</option>
                <option value="Zhemgang">Zhemgang</option>
              </select>
            </div>
          </div>
        </div>

        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Public Address
              </label>
              <input
                type="text"
                required
                value={pub}
                placeholder="Enter Public Address"
                onChange={(e) => setPublic(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Private Address
              </label>
              <input
                type="text"
                required
                value={pri}
                placeholder="Enter Private Address"
                onChange={(e) => setPrivate(e.target.value)}
                class="form-control"
              />
            </div>
          </div>
        </div>

        <button type="submit" className="btn btn-primary">
          Add FP
        </button>
      </form>
      <br></br>
      {/* Ternary Operator to show status */}
      {status ? (
        <div
          class="alert alert-success alert-dismissible fade show"
          role="alert"
        >
          {status}
          <button
            type="button"
            class="btn-close"
            data-bs-dismiss="alert"
            aria-label="Close"
          ></button>
        </div>
      ) : (
        <div>{status}</div>
      )}
    </React.Fragment>
  );
}

export default AddSchoolFocal;
