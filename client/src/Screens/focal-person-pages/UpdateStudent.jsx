import React from "react";
import { useParams } from "react-router-dom";
import Axios from "axios";
import { useEffect } from "react";
import FocalPersonHeader from "./FocalPersonHeader";

export default function UpdateStudent() {
  const [fname, setFName] = React.useState("");
  const [mname, setMName] = React.useState("");
  const [lname, setLName] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [dob, setDob] = React.useState("");
  const [school, setSchool] = React.useState("");
  const [stream, setStream] = React.useState("");
  const [pub, setPublic] = React.useState("");
  const [pri, setPrivate] = React.useState("");
  const [status, setStatus] = React.useState("");

  const { index } = useParams();
  // Run only when we have the index
  useEffect(() => {
    Axios.get(`http://localhost:3001/get/${index}`).then((response) => {
      setFName(response.data[0].fname);
      setMName(response.data[0].mname);
      setLName(response.data[0].lname);
      setEmail(response.data[0].email);
      setDob(response.data[0].dob);
      setSchool(response.data[0].school);
      setStream(response.data[0].stream);
      setPublic(response.data[0].public);
      setPrivate(response.data[0].private);
    });
  }, [index]);

  // Add User Function
  const updateUser = (event) => {
    // call route.js
    Axios.put(`http://localhost:3001/put/${index}`, {
      fname: fname,
      mname: mname,
      lname: lname,
      email: email,
      dob: dob,
      school: school,
      stream: stream,
    }).then((response) => {
      // console.log(response);
      if (response.data.error) {
        setStatus(response.data.error);
      } else {
        setStatus(response.data.success);
        setTimeout(() => (window.location.href = "/view-students"), 500);
      }
    });
  };

  return (
    <React.Fragment>
      <FocalPersonHeader></FocalPersonHeader>
      <hr></hr>
      <h6>You are updating details for index no: {index}</h6>
      <hr></hr>
      {/* Ternary Operator to show status */}
      {status ? (
        <div
          class="alert alert-success alert-dismissible fade show"
          role="alert"
        >
          {status}
          <button
            type="button"
            class="btn-close"
            data-bs-dismiss="alert"
            aria-label="Close"
          ></button>
        </div>
      ) : (
        <div>
          {status} <hr></hr>
        </div>
      )}
      {/* End of ternary */}
      <form>
        {/* <!-- 2 column grid layout with text inputs for the first and last names --> */}
        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example1">
                First Name
              </label>
              <input
                type="text"
                required
                value={fname || ""}
                placeholder="Enter first name"
                onChange={(e) => setFName(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example1">
                Middle Name
              </label>
              <input
                type="text"
                value={mname}
                placeholder="Enter middle name"
                onChange={(e) => setMName(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Last Name
              </label>
              <input
                type="text"
                value={lname}
                placeholder="Enter last name"
                onChange={(e) => setLName(e.target.value)}
                class="form-control"
              />
            </div>
          </div>
        </div>

        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example1">
                Email
              </label>
              <input
                type="email"
                required
                value={email}
                placeholder="Enter email"
                onChange={(e) => setEmail(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Date of Birth
              </label>
              <input
                type="date"
                required
                value={dob}
                placeholder="Enter DOB"
                onChange={(e) => setDob(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                School
              </label>
              <input
                type="text"
                required
                value={school}
                readOnly
                onLoad={(e) => setSchool(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Stream
              </label>
              <select
                required
                onChange={(e) => setStream(e.target.value)}
                class="form-select"
                aria-label="Default select example"
              >
                <option value="">Select stream</option>
                <option value="Science">Science</option>
                <option value="Commerce">Commerce</option>
                <option value="Arts">Arts</option>
              </select>
              <span>{stream}</span>
            </div>
          </div>
        </div>

        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Public Address
              </label>
              <input
                type="text"
                readOnly
                value={pub}
                placeholder="Enter Public Address"
                onChange={(e) => setPublic(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Private Address
              </label>
              <input
                type="text"
                readOnly
                value={pri}
                placeholder="Enter Private Address"
                onChange={(e) => setPrivate(e.target.value)}
                class="form-control"
              />
            </div>
          </div>
        </div>

        {/* <!-- Submit button --> */}
        <button onClick={updateUser} type="button" className="btn btn-primary">
          Update Student
        </button>
      </form>
      <br></br>
    </React.Fragment>
  );
}
