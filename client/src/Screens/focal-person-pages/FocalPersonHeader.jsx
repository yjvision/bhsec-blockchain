import React from "react";
import "bootstrap/dist/js/bootstrap.min.js";
import Cookies from "universal-cookie";

const CryptoJS = require("crypto-js");
const secretPass = "XkhZG4fW2t2W";

function FocalPersonHeader() {
  const cookies = new Cookies();

  const ciphertext = cookies.get("cookies");

  // If the ciphertext is not defined, go to login page
  if (!ciphertext) {
    window.location.href = "/";
  }
  // Decryption
  var bytes = CryptoJS.AES.decrypt(ciphertext, secretPass);
  var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

  // assign the decrypted value
  var username = decryptedData[0]["username"];
  var address = decryptedData[0]["address"];
  var school = decryptedData[0]["school"];

  // Logout
  const logout = () => {
    cookies.remove("cookies");
    window.location.href = "/";
  };

  return (
    <React.Fragment>
      <nav className="navbar navbar-expand-md bg-dark navbar-dark sticky-top">
        <div className="container-fluid">
          <a className="navbar-brand" href="/focal-person">
            Focal Person : {school}
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Students
                </a>
                <ul className="dropdown-menu">
                  <li>
                    <a className="dropdown-item" href="/add-students">
                      Add Students
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="/view-students">
                      View Students
                    </a>
                  </li>
                </ul>
              </li>

              {/* <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  role="button"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                >
                  Marks Entry
                </a>
                <ul className="dropdown-menu">
                  <li>
                    <a className="dropdown-item" href="/add-science-marks">
                      Science
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="/add-commerce-marks">
                      Commerce
                    </a>
                  </li>
                  <li>
                    <a className="dropdown-item" href="/add-arts-marks">
                      Arts
                    </a>
                  </li>
                </ul>
              </li> */}
            </ul>

            <button
              type="button"
              className="btn btn-danger btn-sm"
              onClick={logout}
            >
              Logout
            </button>
          </div>
        </div>
      </nav>
      <b>Your Employee ID: </b> {username}
      <br></br>
      <b>Your Public Address: </b> {address}
    </React.Fragment>
  );
}

export default FocalPersonHeader;
