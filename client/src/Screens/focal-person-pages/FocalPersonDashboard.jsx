import React from "react";
import FocalPersonHeader from "./FocalPersonHeader";

function FocalPersonDashboard() {
  return (
    <React.Fragment>
      <FocalPersonHeader></FocalPersonHeader>
      <hr></hr>
      <h3>Display the summary of the records</h3>
    </React.Fragment>
  );
}

export default FocalPersonDashboard;
