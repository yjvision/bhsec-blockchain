import React from "react";
import Axios from "axios";
import { Link } from "react-router-dom";
import FocalPersonHeader from "./FocalPersonHeader";
import Cookies from "universal-cookie";

const CryptoJS = require("crypto-js");
const secretPass = "XkhZG4fW2t2W";

export default function ViewStudents() {
  const [users, setUsers] = React.useState([]);
  const [status, setStatus] = React.useState("");

  const cookies = new Cookies();
  const ciphertext = cookies.get("cookies");
  // Decryption
  var bytes = CryptoJS.AES.decrypt(ciphertext, secretPass);
  var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

  // assign the decrypted value
  var schoolDecrypted = decryptedData[0]["school"];

  // Call the function when the page loads
  React.useEffect(() => {
    viewUsers();
  }, []);

  // View Users Function
  const viewUsers = () => {
    Axios.get(`http://localhost:3001/get-students/${schoolDecrypted}`).then(
      (response) => {
        if (response.data.error) {
          setStatus(response.data.error);
        } else {
          setUsers(response.data);
        }
      }
    );
  };

  return (
    <React.Fragment>
      <FocalPersonHeader></FocalPersonHeader>
      <hr></hr>
      {/* Ternary Operator to show status */}
      {status ? (
        <div
          class="alert alert-success alert-dismissible fade show"
          role="alert"
        >
          {status}
          <button
            type="button"
            class="btn-close"
            data-bs-dismiss="alert"
            aria-label="Close"
          ></button>
        </div>
      ) : (
        <div>{status}</div>
      )}
      <div className="table-responsive">
        <table className="table table-hover table-striped" id="userTable">
          <thead className="table-light">
            <th>Public Address</th>
            <th>Name</th>
            <th>Email</th>
            <th>Index</th>
            <th>School</th>
            <th>Stream</th>
            <th>EDIT</th>
            <th>DELETE</th>
          </thead>

          <tbody>
            {users.map((user) => {
              return (
                <tr key={user.indexN}>
                  <td>{user.public}</td>
                  <td>{user.fname}</td>
                  <td>{user.email}</td>
                  <td>{user.indexN}</td>
                  <td>{user.school}</td>
                  <td>{user.stream}</td>
                  <td>
                    <Link to={`/get/${user.indexN}`}>
                      <button className="btn btn-primary btn-sm">Update</button>
                    </Link>
                  </td>
                  <td>
                    <Link>
                      <button className="btn btn-danger btn-sm">Delete</button>
                    </Link>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </React.Fragment>
  );
}
