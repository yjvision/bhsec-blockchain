import React from "react";
import Axios from "axios";
import FocalPersonHeader from "./FocalPersonHeader";
import Cookies from "universal-cookie";

const CryptoJS = require("crypto-js");
const secretPass = "XkhZG4fW2t2W";

export default function AddStudents() {
  const [fname, setFName] = React.useState("");
  const [mname, setMName] = React.useState("");
  const [lname, setLName] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [indexN, setindexN] = React.useState("");
  const [dob, setDob] = React.useState("");
  const [school, setSchool] = React.useState([null]);
  const [stream, setStream] = React.useState("");
  const [pub, setPublic] = React.useState("");
  const [pri, setPrivate] = React.useState("");
  const [status, setStatus] = React.useState("");

  const cookies = new Cookies();

  const ciphertext = cookies.get("cookies");
  // Decryption
  var bytes = CryptoJS.AES.decrypt(ciphertext, secretPass);
  var decryptedData = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));

  // assign the decrypted value
  var schoolDecrypted = decryptedData[0]["school"];

  // Add User Function
  const addUser = (event) => {
    event.preventDefault(); // prevent page refresh

    setFName("");
    setMName("");
    setLName("");
    setEmail("");
    setindexN("");
    setDob("");
    setSchool("");
    setStream("");
    setPrivate("");
    setPublic("");

    // call server.js
    Axios.post("http://localhost:3001/add-users", {
      fname: fname,
      mname: mname,
      lname: lname,
      email: email,
      indexN: indexN,
      dob: dob,
      school: schoolDecrypted,
      stream: stream,
      pub: pub,
      pri: pri,
    }).then((response) => {
      if (response.data.error) {
        setStatus(response.data.error);
      } else {
        setStatus(response.data.success);
      }
    });
  };

  // const bulkUpload = () => {
  //   Axios.post("http://localhost:3001/uploadfile", {
  //     // file: uploadfile
  //     }).then((response) => {
  //         console.log(response);
  //         // if(response.data.error){
  //         //   setStatus(response.data.error);
  //         // }else{
  //         //   setStatus(response.data.success);
  //         // }
  //     });
  // }

  return (
    <React.Fragment>
      <FocalPersonHeader></FocalPersonHeader>
      <hr></hr>
      <h4 style={{ textAlign: "center" }}>Add Students [{schoolDecrypted}]</h4>
      <hr></hr>
      {/* Add User Details */}

      <form onSubmit={addUser}>
        {/* <!-- 2 column grid layout with text inputs for the first and last names --> */}
        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example1">
                First Name
              </label>
              <input
                type="text"
                required
                value={fname}
                placeholder="Enter first name"
                onChange={(e) => setFName(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example1">
                Middle Name
              </label>
              <input
                type="text"
                value={mname}
                placeholder="Enter middle name"
                onChange={(e) => setMName(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Last Name
              </label>
              <input
                type="text"
                value={lname}
                placeholder="Enter last name"
                onChange={(e) => setLName(e.target.value)}
                class="form-control"
              />
            </div>
          </div>
        </div>

        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example1">
                Email
              </label>
              <input
                type="email"
                required
                value={email}
                placeholder="Enter email"
                onChange={(e) => setEmail(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example1">
                Index Number
              </label>
              <input
                type="number"
                required
                value={indexN}
                placeholder="Enter Index Number"
                onChange={(e) => setindexN(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Date of Birth
              </label>
              <input
                type="date"
                required
                value={dob}
                placeholder="Enter DOB"
                onChange={(e) => setDob(e.target.value)}
                class="form-control"
              />
            </div>
          </div>
        </div>

        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                School
              </label>
              <input
                type="text"
                required
                value={schoolDecrypted}
                readOnly
                onLoad={(e) => setSchool(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Stream
              </label>
              <select
                required
                onChange={(e) => setStream(e.target.value)}
                class="form-select"
                aria-label="Default select example"
              >
                <option value="">Select stream</option>
                <option value="Science">Science</option>
                <option value="Commerce">Commerce</option>
                <option value="Arts">Arts</option>
              </select>
            </div>
          </div>
        </div>

        <div class="row mb-4">
          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Public Address
              </label>
              <input
                type="text"
                required
                value={pub}
                placeholder="Enter Public Address"
                onChange={(e) => setPublic(e.target.value)}
                class="form-control"
              />
            </div>
          </div>

          <div class="col">
            <div class="form-outline">
              <label class="form-label" for="form6Example2">
                Private Address
              </label>
              <input
                type="text"
                required
                value={pri}
                placeholder="Enter Private Address"
                onChange={(e) => setPrivate(e.target.value)}
                class="form-control"
              />
            </div>
          </div>
        </div>

        <button type="submit" className="btn btn-primary">
          Add User
        </button>
      </form>
      <br></br>

      {/* Ternary Operator to show status */}
      {status ? (
        <div
          class="alert alert-success alert-dismissible fade show"
          role="alert"
        >
          {status}
          <button
            type="button"
            class="btn-close"
            data-bs-dismiss="alert"
            aria-label="Close"
          ></button>
        </div>
      ) : (
        <div>{status}</div>
      )}

      {/* <form action="http://localhost:3001/uploadfile" enctype="multipart/form-data" method="post"> */}
      {/* <form
        action="http://localhost:3001/"
        method="post"
        enctype="multipart/form-data"
      >
        <b>Bulk Upload</b>
        <input
          name="uploadfile"
          accept="csv"
          class="form-control"
          required
          type="file"
        />
        <button type="submit" className="btn btn-primary">
          Bulk Upload
        </button>
      </form>
      <br></br> */}
    </React.Fragment>
  );
}
