import React from "react";
// import {useLocation} from 'react-router-dom';
import "bootstrap/dist/js/bootstrap.min.js";
import Cookies from "universal-cookie";

export default function StudentHeader() {
  const cookies = new Cookies();

  // If the session is out, go to login page
  const firstname = cookies.get("firstname");
  const middlename = cookies.get("middlename");
  const lastname = cookies.get("lastname");
  const school = cookies.get("school");
  const address = cookies.get("address");
  const index = cookies.get("index");
  const stream = cookies.get("stream");

  if (!stream || !index || !address || !school || !firstname) {
    window.location.href = "/student-login";
  }

  const logout = () => {
    cookies.remove("firstname");
    cookies.remove("middlename");
    cookies.remove("lastname");
    cookies.remove("school");
    cookies.remove("address");
    cookies.remove("index");
    window.location.href = "/student-login";
  };

  return (
    <div>
      <nav className="navbar navbar-expand-md bg-dark navbar-dark">
        <div className="container-fluid">
          <a className="navbar-brand" href="/admin">
            Student Dashboard
          </a>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              {/* <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                            Users
                        </a>
                        <ul className="dropdown-menu">
                            <li><a className="dropdown-item" href="/add-users">Add Users</a></li>
                            <li><a className="dropdown-item" href="/view-users">View Users</a></li>
                        </ul>
                    </li> */}
            </ul>

            <button
              type="button"
              className="btn btn-danger btn-sm"
              onClick={logout}
            >
              Logout
            </button>
          </div>
        </div>
      </nav>

      <b>STUDENT NAME: </b>
      <span className="text-uppercase">
        {firstname} {middlename} {lastname}
      </span>
      <br></br>
      <b>SCHOOL: </b>
      <span className="text-uppercase">{school}</span>
      <br></br>
      <b>STREAM: </b>
      <span className="text-uppercase">{stream}</span>
      <br></br>
      <b>Index Number: </b>
      <span className="text-uppercase">{index}</span>
      <br></br>
      <b>ADDRESS: </b>
      <span>{address}</span>
    </div>
  );
}
