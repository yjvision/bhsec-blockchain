import * as React from "react";
import Axios from "axios";
import { useNavigate } from "react-router-dom";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Header from "./GeneralHeader";
import Cookies from "universal-cookie";

export default function StudentLogin() {
  const cookies = new Cookies();

  const [dob, setDob] = React.useState("");
  const [index, setIndex] = React.useState("");
  const [status, setStatus] = React.useState("");
  const [defaultAccount, setDefaultAcocunt] = React.useState("");

  const navigate = useNavigate();

  // Connect to wallet
  const connectWalletHandler = () => {
    if (window.ethereum && window.ethereum.isMetaMask) {
      // console.log('MetaMask Here!');
      window.ethereum
        .request({ method: "eth_requestAccounts" })
        .then((result) => {
          accountChangedHandler(result[0]);
        })
        .catch((error) => {
          setStatus(error.message);
        });
    } else {
      // console.log('Need to install MetaMask');
      alert("Please install MetaMask browser extension to interact");
    }
  };

  const accountChangedHandler = (newAccount) => {
    setDefaultAcocunt(newAccount);
  };

  // Login function
  const StudentLogin = (event) => {
    console.log(dob);
    console.log(index);
    console.log(defaultAccount);
    event.preventDefault(); // prevent page refresh

    setDob("");
    setIndex("");
    setDefaultAcocunt("");

    // call route.js
    Axios.post("http://localhost:3001/student-login", {
      address: defaultAccount,
      index: index,
      dob: dob,
    }).then((response) => {
      if (response.data.message) {
        setStatus(response.data.message);
      } else {
        cookies.set("index", response.data[0].indexN);
        cookies.set("firstname", response.data[0].fname);
        cookies.set("middlename", response.data[0].mname);
        cookies.set("lastname", response.data[0].lname);
        cookies.set("school", response.data[0].school);
        cookies.set("stream", response.data[0].stream);
        cookies.set("address", response.data[0].public);

        navigate("/student");
      }
    });
  };

  return (
    <div className="container">
      <Header></Header>
      <div className="row d-flex justify-content-center">
        <div className="col-md-5">
          <h5>
            <strong>
              <center>Student Login</center>
            </strong>
          </h5>
          <hr></hr>
          <div className="w-100 p-1 alert alert-danger" role="alert">
            <small>Connect to MetaMask and then enter your credentials.</small>
          </div>
          <hr></hr>
          <button
            onClick={connectWalletHandler}
            className="btn btn-primary bt-sm"
          >
            Connect to MetaMask
          </button>
          <hr></hr>
          <h7>
            <strong>Account Address:</strong> {defaultAccount}{" "}
          </h7>
          <hr></hr>

          <form>
            <div className="mb-3">
              <label>Index Number</label>
              <input
                type="number"
                className="form-control"
                placeholder="Enter Index no"
                value={index}
                required
                // grab the input and set to the state
                onChange={(e) => {
                  setIndex(e.target.value);
                }}
              />
            </div>

            <div className="mb-3">
              <label>Password (DoB)</label>
              <input
                type="date"
                required
                value={dob}
                className="form-control"
                onChange={(e) => {
                  setDob(e.target.value);
                }}
              />
            </div>

            <div className="d-grid">
              <button
                onClick={StudentLogin}
                type="button"
                className="btn btn-primary"
              >
                Login
              </button>
            </div>
            {/* <p className="forgot-password text-right">
                    Forgot <a href="http:www.gcit.rub.edu.bt">password?</a>
                </p> */}
          </form>
          <hr></hr>

          {/* Ternary Operator to show status */}
          {status ? (
            <div
              class="alert alert-success alert-dismissible fade show"
              role="alert"
            >
              {status}
              <button
                type="button"
                class="btn-close"
                data-bs-dismiss="alert"
                aria-label="Close"
              ></button>
            </div>
          ) : (
            <div>{status}</div>
          )}
        </div>
      </div>
    </div>
  );
}
