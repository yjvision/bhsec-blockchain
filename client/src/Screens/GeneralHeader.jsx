import React, {Component} from 'react';
import "bootstrap/dist/js/bootstrap.min.js";

export default class Header extends Component {
    render(){
        return(
            <nav className="navbar navbar-expand-md bg-dark navbar-dark">
            <div className="container-fluid">
                <a className="navbar-brand" href="/">BHSEC Result Repository Blockchain</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav">
                        <li className="nav-item">
                        <a className="nav-link" href="/">BCSEA & School</a>
                        </li>
                    </ul>

                    <ul className="navbar-nav">
                        <li className="nav-item">
                        <a className="nav-link" href="/student-login">Student</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        )
    }
}