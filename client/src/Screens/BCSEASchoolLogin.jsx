import * as React from "react";
import Axios from "axios";
import { useNavigate } from "react-router-dom";
import "../../node_modules/bootstrap/dist/css/bootstrap.min.css";
import Header from "./GeneralHeader";
import Cookies from "universal-cookie";

const CryptoJS = require("crypto-js");
const secretPass = "XkhZG4fW2t2W";

export default function LogIn() {
  const cookies = new Cookies();

  const [username, setUsername] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [role, setRole] = React.useState("");
  const [status, setStatus] = React.useState("");
  const [defaultAccount, setDefaultAcocunt] = React.useState("");

  const navigate = useNavigate();

  // Connect to wallet
  const connectWalletHandler = () => {
    if (window.ethereum && window.ethereum.isMetaMask) {
      // console.log('MetaMask Here!');
      window.ethereum
        .request({ method: "eth_requestAccounts" })
        .then((result) => {
          accountChangedHandler(result[0]);
        })
        .catch((error) => {
          setStatus(error.message);
        });
    } else {
      // console.log('Need to install MetaMask');
      alert("Please install MetaMask browser extension to interact");
    }
  };

  const accountChangedHandler = (newAccount) => {
    setDefaultAcocunt(newAccount);
  };

  Axios.defaults.withCredentials = true;

  // Login function
  const signin = (e) => {
    e.preventDefault();
    if (role === "bcsea") {
      // call server.js for admin login
      Axios.post("http://localhost:3001/admin-login", {
        address: defaultAccount,
        username: username,
        password: password,
      }).then((response) => {
        if (response.data.message) {
          setStatus(response.data.message);
        } else {
          // cookies.set("userId", username);
          // cookies.set("address", defaultAccount);
          var adminData = [
            {
              username: username,
              address: defaultAccount,
            },
          ];
          // Encryption
          var ciphertext = CryptoJS.AES.encrypt(
            JSON.stringify(adminData),
            secretPass
          ).toString();

          cookies.set("cookies", ciphertext);
          navigate("/admin");
        }
      });
    } else {
      // Call server.js for focal person login
      Axios.post("http://localhost:3001/focal-login", {
        address: defaultAccount,
        username: username,
        password: password,
      }).then((response) => {
        if (response.data.message) {
          setStatus(response.data.message);
        } else {
          var focalPersonData = [
            {
              username: username,
              address: defaultAccount,
              school: response.data[0]["school"],
            },
          ];
          // Encryption
          var ciphertext = CryptoJS.AES.encrypt(
            JSON.stringify(focalPersonData),
            secretPass
          ).toString();

          cookies.set("cookies", ciphertext);
          navigate("/focal-person");
        }
      });
    }
  };

  return (
    <div className="container">
      <Header></Header>
      <div className="row d-flex justify-content-center">
        <div className="col-md-5">
          <h5>
            <strong>
              <center>Admin Login</center>
            </strong>
          </h5>
          <hr></hr>
          <div className="w-100 p-1 alert alert-danger" role="alert">
            <small>Connect to MetaMask and then enter your credentials.</small>
          </div>
          <hr></hr>
          <button
            onClick={connectWalletHandler}
            className="btn btn-primary bt-sm"
          >
            Connect to MetaMask
          </button>
          <hr></hr>
          <h7>
            <strong>Account Address:</strong> {defaultAccount}{" "}
          </h7>
          <hr></hr>

          <form onSubmit={signin}>
            <div className="mb-3">
              <label>Username</label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter username"
                required
                // grab the input and set to the state
                onChange={(e) => {
                  setUsername(e.target.value);
                }}
              />
            </div>
            <div className="mb-3">
              <label>Password</label>
              <input
                type="password"
                required
                autoComplete="on"
                className="form-control"
                placeholder="Enter password"
                onChange={(e) => {
                  setPassword(e.target.value);
                }}
              />
            </div>
            <div className="mb-3">
              <label>User Type</label>
              <select
                required
                class="form-select"
                onChange={(e) => setRole(e.target.value)}
                aria-label="Default select example"
              >
                <option value="">Select Role</option>
                <option value="bcsea">BCSEA</option>
                <option value="school-focal">School Focal</option>
              </select>
            </div>

            <div className="d-grid">
              <button type="submit" className="btn btn-primary">
                Login
              </button>
            </div>
          </form>
          <hr></hr>
          {/* Ternary Operator to show status */}
          {status ? (
            <div
              class="alert alert-success alert-dismissible fade show"
              role="alert"
            >
              {status}
              <button
                type="button"
                class="btn-close"
                data-bs-dismiss="alert"
                aria-label="Close"
              ></button>
            </div>
          ) : (
            <div>{status}</div>
          )}
        </div>
      </div>
    </div>
  );
}
