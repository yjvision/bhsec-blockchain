// Contract address
export const SCIENCE_CONTRACT_ADDRESS = '0x8E4aF0E83322aa80F964c0ccE6694f50eF3a7E64'

// ABI
export const SCIENCE_ABI = [
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_bcseaOrg",
          "type": "address"
        }
      ],
      "stateMutability": "nonpayable",
      "type": "constructor"
    },
    {
      "anonymous": false,
      "inputs": [
        {
          "indexed": false,
          "internalType": "address",
          "name": "_stdAddress",
          "type": "address"
        },
        {
          "indexed": false,
          "internalType": "string",
          "name": "stream",
          "type": "string"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "eng1_com",
          "type": "uint256"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "eng2_com",
          "type": "uint256"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "dzo1_com",
          "type": "uint256"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "dzo2_com",
          "type": "uint256"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "che_com",
          "type": "uint256"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "phy_com",
          "type": "uint256"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "bio_opt",
          "type": "uint256"
        },
        {
          "indexed": false,
          "internalType": "uint256",
          "name": "math_opt",
          "type": "uint256"
        }
      ],
      "name": "StudentMarksCreate",
      "type": "event"
    },
    {
      "inputs": [],
      "name": "bcseaOrg",
      "outputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [],
      "name": "studentCount",
      "outputs": [
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "",
          "type": "address"
        }
      ],
      "name": "studentsArr",
      "outputs": [
        {
          "internalType": "address",
          "name": "_stdAddress",
          "type": "address"
        },
        {
          "internalType": "string",
          "name": "stream",
          "type": "string"
        },
        {
          "internalType": "uint256",
          "name": "eng1_com",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "eng2_com",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "dzo1_com",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "dzo2_com",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "che_com",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "phy_com",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "bio_opt",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "math_opt",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_stdAddress",
          "type": "address"
        },
        {
          "internalType": "string",
          "name": "_stream",
          "type": "string"
        },
        {
          "internalType": "uint256",
          "name": "_eng1_com",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "_eng2_com",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "_dzo1_com",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "_dzo2_com",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "_che_com",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "_phy_com",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "_bio_opt",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "_math_opt",
          "type": "uint256"
        }
      ],
      "name": "registerStudentMarks",
      "outputs": [],
      "stateMutability": "nonpayable",
      "type": "function"
    },
    {
      "inputs": [
        {
          "internalType": "address",
          "name": "_stdAddress",
          "type": "address"
        }
      ],
      "name": "getStudentDetails",
      "outputs": [
        {
          "internalType": "string",
          "name": "",
          "type": "string"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        },
        {
          "internalType": "uint256",
          "name": "",
          "type": "uint256"
        }
      ],
      "stateMutability": "view",
      "type": "function",
      "constant": true
    }
  ]