// import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import LogIn from "./Screens/BCSEASchoolLogin";
import AdminDashboard from "./Screens/admin-pages/AdminDashboard";
import StudentLogin from "./Screens/StudentLogin";
import AddStudents from "./Screens/focal-person-pages/AddStudents";
import StudentDashboard from "./Screens/student-pages/StudentDashboard";

import AddScienceMarks from "./Screens/admin-pages/AddScienceMarks";
import AddCommerceMarks from "./Screens/admin-pages/AddCommerceMarks";
import AddArtsMarks from "./Screens/admin-pages/AddArtsMarks";
import AddSchoolFocal from "./Screens/admin-pages/AddSchoolFocal";
import FocalPersonDashboard from "./Screens/focal-person-pages/FocalPersonDashboard";
import ViewStudents from "./Screens/focal-person-pages/ViewStudents";
import UpdateStudent from "./Screens/focal-person-pages/UpdateStudent";
import ViewFocalPerson from "./Screens/admin-pages/ViewFocalPerson";
import AddSchool from "./Screens/admin-pages/AddSchool";
import ViewSchool from "./Screens/admin-pages/ViewSchool";
import UpdateSchool from "./Screens/admin-pages/UpdateSchool";

function App() {
  return (
    <div className="App">
      <div className="container">
        <BrowserRouter>
          <Routes>
            <Route exact path="/" element={<LogIn />} />
            {/* Admin Page */}
            <Route path="/admin" element={<AdminDashboard />} />
            <Route
              path="/focal-person"
              element={<FocalPersonDashboard />}
            ></Route>
            <Route path="/student-login" element={<StudentLogin />} />
            <Route path="/student" element={<StudentDashboard />} />
            <Route path="/add-students" element={<AddStudents />} />
            <Route path="add-school-focal" element={<AddSchoolFocal />}></Route>
            <Route
              path="/view-focal-person"
              element={<ViewFocalPerson />}
            ></Route>
            <Route path="/view-students" element={<ViewStudents />} />

            <Route path="/add-school" element={<AddSchool />}></Route>
            <Route path="/view-school" element={<ViewSchool />}></Route>
            <Route path="/getschool/:sch" element={<UpdateSchool />} />

            <Route path="/add-science-marks" element={<AddScienceMarks />} />
            <Route
              path="/add-commerce-marks"
              element={<AddCommerceMarks />}
            ></Route>
            <Route path="/add-arts-marks" element={<AddArtsMarks />}></Route>
            <Route path="/get/:index" element={<UpdateStudent />} />
          </Routes>
        </BrowserRouter>
      </div>
    </div>
  );
}

export default App;
