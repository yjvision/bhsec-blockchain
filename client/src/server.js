const express = require("express");
const mysql = require("mysql");
const cors = require("cors");
const app = express();
// const csv = require("fast-csv");
const bodyparser = require("body-parser");
const multer = require("multer");
const path = require("path");
const fs = require("fs");
const nodemailer = require("nodemailer");

app.use(express.json());

// const whitelist = ["http://localhost:3000"]

// const corsOptions = {
//   origin: function (origin, callback) {
//     if (!origin || whitelist.indexOf(origin) !== -1) {
//       callback(null, true)
//     } else {
//       callback(new Error("Not allowed by CORS"))
//     }
//   },
//   credentials: true,
// }

app.use(
  cors({
    origin: ["http://localhost:3000"],
    methods: ["GET", "POST", "PUT"],
    // allowing cookies to be enabled
    credentials: true,
  })
);

// body-parser middleware use
app.use(bodyparser.json());
app.use(
  bodyparser.urlencoded({
    extended: true,
  })
);

var pool = mysql.createPool({
  connectionLimit: 10,
  host: "localhost",
  user: "root",
  password: "root",
  database: "bhsec_db",
});

pool.getConnection(function (err, connection) {
  if (err) {
    console.log(err);
  }
});

//! Use of Multer
var storage = multer.diskStorage({
  destination: (req, file, callBack) => {
    callBack(null, "./uploads/");
  },
  filename: (req, file, callBack) => {
    callBack(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  },
});

var upload = multer({
  storage: storage,
});

// upload csv to database
// app.post("/uploadfile", upload.single("uploadfile"), (req, res) => {
//   UploadCsvDataToMySQL(__dirname + "/uploads/" + req.file.filename);
//   console.log("CSV file data has been uploaded in mysql database");
// });

// function UploadCsvDataToMySQL(filePath) {
//   let stream = fs.createReadStream(filePath);
//   let csvData = [];
//   let csvStream = csv
//     .parse()
//     .on("data", function (data) {
//       csvData.push(data);
//     })
//     .on("end", function () {
//       // Remove Header ROW
//       csvData.shift();

//       let query =
//         "INSERT INTO users_details (fname, mname, lname, email, indexN, dob, school, stream, public, private) VALUES ?";
//       pool.query(query, [csvData], (error, response) => {
//         console.log(error || response);
//       });

//       // delete file after saving to MySQL database
//       // -> you can comment the statement to see the uploaded CSV file.
//       fs.unlinkSync(filePath);
//     });

//   stream.pipe(csvStream);
// }

// ************* API FOR LOGINS ***************
// API to check whether the user already login
app.get("/login", (req, res) => {
  if (req.session.user) {
    return res.send({ loggedIn: true, user: req.session.user });
  } else {
    return res.send({ loggedIn: false, user: req.session.user });
  }
});

// API for admin login route
app.post("/admin-login", (req, res) => {
  const address = req.body.address;
  const username = req.body.username;
  const password = req.body.password;

  if (username && password && address) {
    pool.query(
      "SELECT * FROM users WHERE username=? AND password=? AND public=?",
      [username, password, address],
      (err, result) => {
        if (err) {
          return res.send({ err: err });
        }
        if (result.length > 0) {
          return res.send(result);
        } else {
          return res.send({ message: "Incorrect username/password" });
        }
      }
    );
  } else {
    return res.send({ message: "Please enter username and password" });
  }
});

// API for focal person login route
app.post("/focal-login", (req, res) => {
  const address = req.body.address;
  const empId = req.body.username;
  const password = req.body.password;

  if (empId && password && address) {
    pool.query(
      "SELECT * FROM school_focal WHERE empID=? AND password=? AND public=?",
      [empId, password, address],
      (err, result) => {
        if (err) {
          return res.send({ err: err });
        }
        if (result.length > 0) {
          return res.send(result);
        } else {
          return res.send({ message: "Incorrect username/password" });
        }
      }
    );
  } else {
    return res.send({ message: "Please enter username and password" });
  }
});

// API for student login route
app.post("/student-login", (req, res) => {
  const address = req.body.address;
  const indexN = req.body.index;
  const dob = req.body.dob;

  console.log(address);
  console.log(indexN);
  console.log(dob);

  if (address && indexN && dob) {
    pool.query(
      "SELECT * FROM users_details WHERE indexN=? AND dob=? AND public=?",
      [indexN, dob, address],
      (err, result) => {
        if (err) {
          // console.log(err);
          return res.send({ err: err });
        }
        if (result.length > 0) {
          return res.send(result);
        } else {
          return res.send({ message: "Incorrect username/password/address" });
        }
      }
    );
  } else {
    return res.send({
      message: "Please enter indexN, DOB and generate address",
    });
  }
});

// ************* API FOR ADMIN ***************
// API to add school
app.post("/add-schools", (req, res) => {
  const school = req.body.school;
  const dzongkhag = req.body.dzongkhag;

  if (school && dzongkhag) {
    pool.query(
      "INSERT INTO school (schoolName, dzongkhag) VALUES (?, ?)",
      [school, dzongkhag],
      (err, result) => {
        if (err) {
          return res.send({ success: school + " already exist" });
        } else {
          return res.send({ success: school + " added successfully" });
        }
      }
    );
  } else {
    return res.send({ error: "Please fill the details" });
  }
});

// API to get school based on dzongkhag
app.get("/get-school/:dzo", (req, res) => {
  const { dzo } = req.params;
  pool.query("SELECT * FROM school WHERE dzongkhag=?", dzo, (err, result) => {
    if (result.length == 0) {
      return res.send({ error: "No schools found" });
    } else {
      return res.send(result);
    }
  });
});

// API to get school based on schoolName
app.get("/getschool/:school", (req, res) => {
  const { school } = req.params;
  pool.query(
    "SELECT * FROM school WHERE schoolName=?",
    school,
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        return res.send(result);
      }
    }
  );
});

// API to add school focal person
app.post("/add-focal", (req, res) => {
  const empId = req.body.empId;
  const name = req.body.name;
  const password = req.body.password;
  const email = req.body.email;
  const school = req.body.school;
  const dzongkhag = req.body.dzongkhag;
  const pub = req.body.pub;
  const pri = req.body.pri;

  if (empId && name && password && email && school && dzongkhag && pri && pub) {
    // insert into database
    pool.query(
      "INSERT INTO school_focal (empID, email, name, password, school, dzongkhag, public, private) VALUES (?,?,?,?,?,?,?,?)",
      [empId, email, name, password, school, dzongkhag, pub, pri],
      (err, result) => {
        if (err) {
          return res.send({ error: "Focal person already exisit!" });
        } else {
          // send mail
          var transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
              user: "gyamtsho92@gmail.com",
              pass: "hiyzrzwjcphwbmpg",
            },
          });

          var mailOptions = {
            from: "gyamtsho92@gmail.com",
            to: email,
            subject: "Public and Private Address Details for Meta Mask Setup",
            html:
              "<b>Dear " +
              name +
              "</b>," +
              "<br><b>Your Login Credentials</b>" +
              "<br> <b>Username: </b>" +
              empId +
              "<br><b>Password: </b>" +
              password +
              "<br><br> Use the following information to set up the metamask : <ul><li><b>Public Address:</b>" +
              pub +
              "</li> <li><b>Private Address:</b> " +
              pri +
              '</li></ul> <b>Instruction to setup MetaMask: <a href="https://everyrealm.com/blog/education/set-up-metamask">Click Here</a></b> <br><br> BCSEA Team',
          };

          transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
              console.log(error);
            } else {
              console.log("Email sent: " + info.response);
            }
          });
          return res.send({ success: "Focal person added successfully" });
        }
      }
    );
  }
});

// API to populate details based on public address
app.get("/get-one/:pubadd", (req, res) => {
  const { pubadd } = req.params;
  // console.log(pubadd);
  pool.query(
    "SELECT * FROM USERS_DETAILS WHERE public=?",
    pubadd,
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        return res.send(result);
      }
    }
  );
});

// API to get all schools
app.get("/get-all-schools", (req, res) => {
  pool.query("SELECT schoolName FROM school", (err, result) => {
    if (err) {
      console.log(err);
    } else {
      return res.send(result);
    }
  });
});

// API to get focal person based on dzongkhag
app.get("/get-school-person/:dzo", (req, res) => {
  const { dzo } = req.params;
  pool.query(
    "SELECT * FROM school_focal WHERE dzongkhag=?",
    dzo,
    (err, result) => {
      if (result.length == 0) {
        return res.send({ error: "No schools found" });
      } else {
        return res.send(result);
      }
    }
  );
});

// ************* API FOR FOCAL PERSON ***************
// API to add students route
app.post("/add-users", (req, res) => {
  const fname = req.body.fname;
  const mname = req.body.mname;
  const lname = req.body.lname;
  const email = req.body.email;
  const indexN = req.body.indexN;
  const dob = req.body.dob;
  const school = req.body.school;
  const stream = req.body.stream;
  const pub = req.body.pub;
  const pri = req.body.pri;
  console.log(fname);
  console.log(mname);
  console.log(lname);
  console.log(email);
  console.log(indexN);
  console.log(dob);
  console.log(school);
  console.log(stream);
  console.log(pub);
  console.log(pri);

  if (fname && email && indexN && dob && school && stream && pub && pri) {
    pool.query(
      "INSERT INTO users_details (fname, mname, lname, email, indexN, dob, school, stream, public, private) VALUES (?,?,?,?,?,?,?,?,?,?)",
      [fname, mname, lname, email, indexN, dob, school, stream, pub, pri],
      (err, result) => {
        if (err) {
          return res.send({ error: "User already exisits!" });
          // console.log(err);
        } else {
          var transporter = nodemailer.createTransport({
            service: "gmail",
            auth: {
              user: "gyamtsho92@gmail.com",
              pass: "hiyzrzwjcphwbmpg",
            },
          });

          var mailOptions = {
            from: "gyamtsho92@gmail.com",
            to: email,
            subject: "Public and Private Address Details for Meta Mask Setup",
            // text: 'Dear '+fname+', \n Public Address: '+pub+' \n Private Address: '+pri,
            html:
              "<b>Dear " +
              fname +
              "</b>, <br><br> Use the following information to set up the metamask : <ul><li>Public Address: " +
              pub +
              "</li> <li>Private Address: " +
              pri +
              '</li></ul> <b>Instruction to setup MetaMask: <a href="https://everyrealm.com/blog/education/set-up-metamask">Click Here</a></b> <br><br> BCSEA Team ',
          };

          transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
              console.log(error);
            } else {
              console.log("Email sent: " + info.response);
            }
          });
          return res.send({ success: "Users added successfully" });
          // console.log("Users added successfully");
        }
      }
    );
  } else {
    return res.send({ error: "Please fill the details" });
  }
});

// API to update particular student
app.put("/putschool/:sch", (req, res) => {
  const { sch } = req.params;
  const school = req.body.school;
  const dzongkhag = req.body.dzongkhag;

  if (school && dzongkhag) {
    const sqlUpdate =
      "UPDATE school SET schoolName=?, dzongkhag=? WHERE schoolName=?";
    pool.query(sqlUpdate, [school, dzongkhag, sch], (err, result) => {
      if (err) {
        console.log(err);
      } else {
        return res.send({ success: "Details successfully updated" });
      }
    });
  } else {
    return res.send({ error: "Please fill the details" });
  }
});

// API to view all the students based on school
app.get("/get-students/:school", (req, res) => {
  const { school } = req.params;
  // console.log(school);
  pool.query(
    "SELECT * FROM USERS_DETAILS WHERE school=?",
    school,
    (err, result) => {
      if (result.length == 0) {
        return res.send({ error: "No students found" });
      } else {
        return res.send(result);
      }
    }
  );
});

// API to select particular student
app.get("/get/:indexN", (req, res) => {
  const { indexN } = req.params;
  pool.query(
    "SELECT * FROM USERS_DETAILS WHERE indexN=?",
    indexN,
    (err, result) => {
      if (err) {
        console.log(err);
      } else {
        return res.send(result);
      }
    }
  );
});

// API to update particular student
app.put("/put/:indexN", (req, res) => {
  const { indexN } = req.params;
  const fname = req.body.fname;
  const mname = req.body.mname;
  const lname = req.body.lname;
  const email = req.body.email;
  const dob = req.body.dob;
  const school = req.body.school;
  const stream = req.body.stream;

  console.log(fname);
  console.log(mname);
  console.log(lname);
  console.log(email);
  console.log(dob);
  console.log(school);
  console.log(stream);
  console.log(indexN);

  if (fname && email && dob && school && stream) {
    const sqlUpdate =
      "UPDATE USERS_DETAILS SET fname=?, mname=?, lname=?, email=?, dob=?, school=?, stream=? WHERE indexN=?";
    pool.query(
      sqlUpdate,
      [fname, mname, lname, email, dob, school, stream, indexN],
      (err, result) => {
        if (err) {
          console.log(err);
        } else {
          return res.send({ success: "Details successfully updated" });
        }
      }
    );
  } else {
    return res.send({ error: "Please fill the details" });
    // console.log("Error");
  }
});

app.listen(3001, () => {
  console.log("running server");
});
