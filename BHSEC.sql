use bhsec_db;
show tables;

CREATE TABLE USERS (
    id INT PRIMARY KEY NOT NULL AUTO_INCREMENT ,
    username varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    public varchar(255) NOT NULL,
    private varchar(255) NOT NULL
);

DROP TABLE users;

SELECT * FROM USERS;

INSERT INTO USERS (username, password, public, private) VALUES ('bhsec', 'test123', '0x2AF52665943AEF05ccD0fA51f77102C4Ab1FE618', '2589711673696037bd6a57c8db5a3e61db1352fbf092891f0eb1eb766d9ceafb');

ALTER USER 'root'@'localhost' IDENTIFIED BY 'root'; 
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';

-- STUDENT TABLE
CREATE TABLE USERS_DETAILS (
    fname varchar(255) NOT NULL,
    mname varchar(255),
    lname varchar(255),
    email varchar(255) NOT NULL UNIQUE,
    indexN varchar(255) PRIMARY KEY NOT NULL,
    dob varchar(255) NOT NULL,
    school varchar(255) NOT NULL,
    stream varchar(255) NOT NULL,
    public varchar(255) NOT NULL,
    private varchar(255) NOT NULL
);

SELECT * FROM USERS_DETAILS;

-- drop table USERS_DETAILS;
-- truncate table USERS_DETAILS;
DESCRIBE USERS_DETAILS;
DELETE FROM 

SELECT count(fname) FROM users_details;-


delete from users_details where indexN=11504001934;

ALTER TABLE users_details RENAME COLUMN cid TO indexN;

select * from users_details;

drop table users_details;

describe users_details;

DELETE FROM users_details WHERE indexN = "333";


-- STUDENT TABLE
CREATE TABLE school_focal (
    empID varchar(255) PRIMARY KEY NOT NULL,
    email varchar(255) NOT NULL UNIQUE,
    name varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    school varchar(255) NOT NULL,
    dzongkhag varchar(255) NOT NULL,
    public varchar(255) NOT NULL,
    private varchar(255) NOT NULL
);

insert into school_focal values ("E001", "yontenjamtsho.gcit@rub.edu.bt", "Yonten", "test123", "Mongar HSS", "Mongar",  "0xf27eb524b7d3f1cc703c3af7c33d730fec7f3042", "480afb53ab8e6448a5a6785faa0d2146d9fa23e268d0df23f4950b878e2b1633")


describe school_focal;

SELECT * FROM school_focal;

drop table school_focal;


-- STUDENT TABLE
CREATE TABLE school (
    schoolName varchar(255) NOT NULL PRIMARY KEY,
    dzongkhag varchar(255) NOT NULL
);

describe table school;
select  * from school;

delete from school where schoolName = "d";
delete from school where schoolName = "test";

drop table school;

