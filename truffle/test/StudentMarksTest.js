// Here, we are writing a test code to test all the functions that we
// have created for the StudentMarksContract smart contract.
// 1. Deployment
// 2. Adding Marks
// 3. Getting Marks

// First, we will test the success of the deployment of the smart contract by checking
// if there is a valid address created during the deployment.

// Import the StudentMarksContract smart contract
const StudentMarksContract = artifacts.require('./StudentMarksContract.sol')

// Use the contract to write all tests
// variable: account => all accounts in blockchain
contract('StudentMarksContract', (accounts) => {
    // make sure contract is deployed and before we retrived the
    // student marks object for testing
    beforeEach(async () => {
        this.studentMarks = await StudentMarksContract.deployed();
    })

    // Testong the deployed student marks contract
    it('deploys successfully', async() => {
        // Get the address which the student marks object is stored
        const address = await this.studentMarks.address
        // Test for valid address
        isValidAddress(address)
    })

  //Testing the content in the contract
  it('test adding students marks', async () => {
    return StudentMarksContract.deployed().then((instance) => {
      studentMarksInstance = instance;
      studentAddress = '0x40f5fade5a68e3086cb86390f167bdd643a87792';
     return studentMarksInstance.registerStudentMarks(
        studentAddress, "Science", 50, 55, 60, 65, 55, 50, 0, 0);
    })
    .then((transaction) => {
      isValidAddress(transaction.tx)
      isValidAddress(transaction.receipt.blockHash);
      return studentMarksInstance.getStudentDetails(studentAddress);
    })
    .then((student) => {
      assert.equal(student[0], "Science");
      assert.equal(student[1], 50);
      assert.equal(student[2], 55);
      assert.equal(student[3], 60);
      assert.equal(student[4], 65);
      assert.equal(student[5], 55);
      assert.equal(student[6], 50);
      assert.equal(student[7], 0);
      assert.equal(student[8], 0);
    })
  })
})

// This function check if the address is valid
function isValidAddress(address) {
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
}