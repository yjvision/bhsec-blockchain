var StudentScienceMarks = artifacts.require("./StudentScienceMarksContract.sol");
var StudentCommerceMarks = artifacts.require("./StudentCommerceMarksContract.sol");
var StudentArtsMarks = artifacts.require("./StudentArtsMarksContract.sol");

const deployerAddress = '0x2AF52665943AEF05ccD0fA51f77102C4Ab1FE618';

module.exports = function(deployer){
    deployer.deploy(StudentScienceMarks, deployerAddress);
    deployer.deploy(StudentCommerceMarks, deployerAddress);
    deployer.deploy(StudentArtsMarks, deployerAddress);
};