// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract StudentScienceMarksContract {
    uint public studentCount = 0;

    // address of one who deploys the contract
    address public bcseaOrg;

    // STUDENT DATA [SCIENCE]
    struct StudentMarks {
        address _stdAddress;
        string stream;
        uint256  eng1_com;
        uint256  eng2_com;
        uint256  dzo1_com;
        uint256  dzo2_com;
        uint256  che_com;
        uint256  phy_com;
        uint256  bio_opt;
        uint256  math_opt;
    }

    // Save data on blockchain
    event StudentMarksCreate (
        address _stdAddress,
        string stream,
        uint256  eng1_com,
        uint256  eng2_com,
        uint256  dzo1_com,
        uint256  dzo2_com,
        uint256  che_com,
        uint256  phy_com,
        uint256  bio_opt,
        uint256  math_opt
    );

    // mapping address as key to struct student with 
    // mapping students marks
    mapping(address => StudentMarks) public studentsArr;

    //---------- END OF STUDENT DATA

    // assigning the contract deployer as the owner
    // The one which deploy the contract becomes the owner
    constructor(address _bcseaOrg){
        bcseaOrg = _bcseaOrg;
    }

    // function to register student address, course, and marks
    function registerStudentMarks(
        address _stdAddress,
        string memory _stream, 
        uint256 _eng1_com,
        uint256 _eng2_com,
        uint256 _dzo1_com,
        uint256 _dzo2_com,
        uint256 _che_com,
        uint256 _phy_com, 
        uint256  _bio_opt,
        uint256 _math_opt
    ) public {
        // Condition: Make sure that the one 
        // who deployed the contracts adds the data
        require(bcseaOrg == msg.sender, "Only BCSEA can add marks");
        // require(studentsArr[_stdAddress].isExist == false, "Student marks already added");

        StudentMarks storage student = studentsArr[_stdAddress];
        student.stream = _stream;
        student.eng1_com = _eng1_com;
        student.eng2_com = _eng2_com;
        student.dzo1_com = _dzo1_com;
        student.dzo2_com = _dzo2_com;
        student.che_com = _che_com;
        student.phy_com = _phy_com;
        student.bio_opt = _bio_opt;
        student.math_opt = _math_opt;

        emit StudentMarksCreate(
            _stdAddress,
            _stream,
            _eng1_com,
            _eng2_com,
            _dzo1_com,
            _dzo2_com,
            _che_com,
            _phy_com,
            _bio_opt,
            _math_opt
        );
    }

    // function to get the details of a student 
    // when student address is given
    function getStudentDetails(address _stdAddress) 
    public view returns (
        string memory, 
        uint256,
        uint256,
        uint256, 
        uint256,
        uint256, 
        uint256, 
        uint256, 
        uint256 )
        {
        return(
            studentsArr[_stdAddress].stream,
            studentsArr[_stdAddress].eng1_com,
            studentsArr[_stdAddress].eng2_com,
            studentsArr[_stdAddress].dzo1_com,
            studentsArr[_stdAddress].dzo2_com,
            studentsArr[_stdAddress].che_com,
            studentsArr[_stdAddress].phy_com,
            studentsArr[_stdAddress].bio_opt,
            studentsArr[_stdAddress].math_opt
        );
    }
}